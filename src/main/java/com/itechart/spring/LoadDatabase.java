package com.itechart.spring;


import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class LoadDatabase {

    @Bean
    CommandLineRunner initDatabase(EmployeeRepository employeeRepository, OrderRepository orderRepository) {
        return args -> {
            log.info("Preloading" + employeeRepository.save(new Employee("Pasha Orashkov", "burglar")));
            log.info("Preloading" + employeeRepository.save(new Employee("Shsha Leshik", "thief")));
            log.info("Preloading" + orderRepository.save(new Order("MacBook Pro", Status.COMPLETED)));
            log.info("Preloading" + orderRepository.save(new Order("iPhone", Status.IN_PROGRESS)));


        };
    }
}
