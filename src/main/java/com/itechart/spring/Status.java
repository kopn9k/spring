package com.itechart.spring;

public enum Status {

    IN_PROGRESS,
    COMPLETED,
    CANCELLED;
}